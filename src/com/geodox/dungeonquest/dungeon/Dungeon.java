package com.geodox.dungeonquest.dungeon;

import com.geodox.dungeonquest.dungeon.objects.Door;
import com.geodox.flux.util.array.ArrayUtils;
import com.geodox.flux.util.compass.Direction;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.List;

/**
 * Created by GeoDoX on 2017-12-04.
 */
public class Dungeon
{
    public final int sizeX, sizeY;

    private Cell[] cells; // Will eventually be switched to Tiles
    private List<Door> doors;

    public Dungeon(int sizeX, int sizeY)
    {
        this.sizeX = sizeX;
        this.sizeY = sizeY;

        this.cells = new Cell[sizeX * sizeY];

        for (int i = 0; i < this.cells.length; i++)
        {
            this.cells[i] = new Cell();
        }
    }

    public void render(Graphics2D g, int cellSize)
    {
        Cell currentCell;

        for (int y = 0; y < this.sizeY; y++)
        {
            for (int x = 0; x < this.sizeX; x++)
            {
                currentCell = this.cells[ArrayUtils.index2DTo1D(x, y, this.sizeX)];

                g.setColor(Color.lightGray);
                g.fillRect(x * cellSize, y * cellSize, cellSize, cellSize);

                g.setColor(Color.black);

                if (currentCell.isWall(Direction.NORTH))
                    drawLine(g, Direction.NORTH, x, y, cellSize);
                if (currentCell.isWall(Direction.EAST))
                    drawLine(g, Direction.EAST, x, y, cellSize);
                if (currentCell.isWall(Direction.SOUTH))
                    drawLine(g, Direction.SOUTH, x, y, cellSize);
                if (currentCell.isWall(Direction.WEST))
                    drawLine(g, Direction.WEST, x, y, cellSize);

                g.setColor(Color.red);

                if (this.doors != null)
                    for (Door door : doors)
                        if (door.mazeCell() == currentCell)
                            drawLine(g, door.directionToRoomCell(), x, y, cellSize);
            }
        }
    }

    public void cells(Cell[] cells)
    {
        this.cells = cells;
    }

    public Cell[] cells()
    {
        return this.cells;
    }

    public void doors(List<Door> doors)
    {
        this.doors = doors;
    }

    private static void drawLine(Graphics2D g, Direction direction, int x, int y, int cellSize)
    {
        switch (direction)
        {
            case NORTH:
                g.drawLine(x * cellSize, y * cellSize, x * cellSize + cellSize, y * cellSize);
                break;
            case EAST:
                g.drawLine(x * cellSize + cellSize, y * cellSize, x * cellSize + cellSize, y * cellSize + cellSize);
                break;
            case SOUTH:
                g.drawLine(x * cellSize + cellSize, y * cellSize + cellSize, x * cellSize + cellSize, y * cellSize);
                break;
            case WEST:
                g.drawLine(x * cellSize, y * cellSize, x * cellSize, y * cellSize + cellSize);
                break;
        }
    }
}
