package com.geodox.dungeonquest.dungeon;


import com.geodox.flux.util.compass.Direction;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by GeoDoX on 2017-12-04.
 */
public class Cell
{
    public enum Type
    {
        GENERIC,
        ROOM,
        MAZE,
        DUNGEON,
    }

    protected Type type;
    protected Map<Direction, Boolean> walls;
    protected Map<Direction, Cell> neighbors;

    public Cell()
    {
        this.type = Type.GENERIC;

        this.walls = new HashMap<>(0);
        this.walls.put(Direction.NORTH, true);
        this.walls.put(Direction.EAST, true);
        this.walls.put(Direction.SOUTH, true);
        this.walls.put(Direction.WEST, true);

        this.neighbors = new HashMap<>(0);
    }

    public Cell(byte bitmask)
    {
        this.type = Type.GENERIC;

        this.walls = new HashMap<>(0);
        this.neighbors = new HashMap<>(0);

        setWall(Direction.NORTH, (bitmask & directionBitmask(Direction.NORTH)) > 0);
        setWall(Direction.EAST, (bitmask & directionBitmask(Direction.EAST)) > 0);
        setWall(Direction.SOUTH, (bitmask & directionBitmask(Direction.SOUTH)) > 0);
        setWall(Direction.WEST, (bitmask & directionBitmask(Direction.WEST)) > 0);
    }

    public boolean isWall(Direction direction)
    {
        return this.walls.get(direction);
    }

    public void setWall(Direction direction, boolean isWall)
    {
        this.walls.put(direction, isWall);
    }

    public byte wallBitmask()
    {
        byte bitmask = (byte) 0;

        if (isWall(Direction.NORTH))
            bitmask = (byte) (bitmask | directionBitmask(Direction.NORTH));
        if (isWall(Direction.EAST))
            bitmask = (byte) (bitmask | directionBitmask(Direction.EAST));
        if (isWall(Direction.SOUTH))
            bitmask = (byte) (bitmask | directionBitmask(Direction.SOUTH));
        if (isWall(Direction.WEST))
            bitmask = (byte) (bitmask | directionBitmask(Direction.WEST));

        return bitmask;
    }

    public Cell neighbor(Direction direction)
    {
        return this.neighbors.get(direction);
    }

    public void addNeighbor(Direction direction, Cell neighbor)
    {
        this.neighbors.put(direction, neighbor);
    }

    public Map<Direction, Cell> neighbors()
    {
        return this.neighbors;
    }

    private static byte directionBitmask(Direction direction)
    {
        switch (direction)
        {
            case NORTH:
                return (byte) 1;
            case EAST:
                return (byte) 2;
            case SOUTH:
                return (byte) 4;
            case WEST:
                return (byte) 8;
            default:
                return (byte) 0;
        }
    }

    public Type type()
    {
        return this.type;
    }

    public void type(Type type)
    {
        this.type = type;
    }

    public Cell copy()
    {
        Cell c = new Cell();

        c.type = this.type;
        c.walls = new HashMap<>(this.walls);
        c.neighbors = new HashMap<>(this.neighbors);

        return c;
    }

    public boolean equals(Cell c)
    {
        if (this.type != c.type)
            return false;

        if (this.walls.size() != c.walls.size())
            return false;

        if (this.walls.get(Direction.NORTH) != c.walls.get(Direction.NORTH))
            return false;
        if (this.walls.get(Direction.EAST) != c.walls.get(Direction.EAST))
            return false;
        if (this.walls.get(Direction.SOUTH) != c.walls.get(Direction.SOUTH))
            return false;
        if (this.walls.get(Direction.WEST) != c.walls.get(Direction.WEST))
            return false;

        if (this.neighbors.size() != c.neighbors.size())
            return false;

        return true;
    }
}
