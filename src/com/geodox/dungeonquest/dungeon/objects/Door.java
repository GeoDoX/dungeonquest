package com.geodox.dungeonquest.dungeon.objects;

import com.geodox.dungeonquest.dungeon.Cell;
import com.geodox.flux.util.compass.Direction;

/**
 * Created by GeoDoX on 2017-12-04.
 */
public class Door
{
    private Cell mazeCell;
    private Cell roomCell;
    private Direction directionToRoomCell;

    public Door(Cell mazeCell, Cell roomCell, Direction directionToRoomCell)
    {
        this.mazeCell = mazeCell;
        this.roomCell = roomCell;
        this.directionToRoomCell = directionToRoomCell;
    }

    public Cell mazeCell()
    {
        return this.mazeCell;
    }

    public Cell roomCell()
    {
        return this.roomCell;
    }

    public Direction directionToRoomCell()
    {
        return this.directionToRoomCell;
    }
}
