package com.geodox.dungeonquest.dungeon.objects;

import com.geodox.dungeonquest.dungeon.Cell;
import com.geodox.flux.core.math.Vector2;
import com.geodox.flux.util.array.ArrayUtils;
import com.geodox.flux.util.compass.Direction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GeoDoX on 2017-12-04.
 */
public class Room
{
    private Vector2 position;
    private Vector2 size;

    private List<Cell> cells;

    public Room(int x, int y, int width, int height)
    {
        this.position = new Vector2(x, y);
        this.size = new Vector2(width, height);
    }

    public void place(Cell[] cells, int width, int height, List<Cell> unvisitedCells)
    {
        this.cells = new ArrayList<>(0);

        int index;

        boolean northWall;
        boolean eastWall;
        boolean southWall;
        boolean westWall;

        Cell cell;

        for (int y = (int) position.y; y <= position.y + size.y; y++)
        {
            for (int x = (int) position.x; x <= position.x + size.x; x++)
            {
                if (x < width && y < height)
                {
                    index = ArrayUtils.index2DTo1D(x, y, width);

                    northWall = (y == (int) position.y);
                    eastWall = (x == (int) position.x + size.x);
                    southWall = (y == (int) position.y + size.y);
                    westWall = (x == (int) position.x);

                    unvisitedCells.remove(cells[index]);

                    cell = new Cell();
                    cell.type(Cell.Type.ROOM);
                    cell.setWall(Direction.NORTH, northWall);
                    cell.setWall(Direction.EAST, eastWall);
                    cell.setWall(Direction.SOUTH, southWall);
                    cell.setWall(Direction.WEST, westWall);

                    this.cells.add(cell);

                    cells[index] = cell;
                }
            }
        }
    }

    public Vector2 position()
    {
        return this.position;
    }

    public Vector2 size()
    {
        return this.size;
    }

    public boolean containsCell(Cell cell)
    {
        for (Cell c : this.cells)
        {
            if (c.equals(cell))
                return true;
        }

        return false;
    }
}
