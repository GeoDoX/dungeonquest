package com.geodox.dungeonquest.generation.rwm.rooms.bsp;

import com.geodox.flux.util.compass.AxisDirection;

/**
 * Created by GeoDoX on 2017-12-10.
 */
public class BSPNodeInfo
{
    public final int x, y;
    public final int width, height;

    public AxisDirection direction;

    public BSPNodeInfo(int x, int y, int width, int height)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}
