package com.geodox.dungeonquest.generation.rwm.rooms.bsp;

import com.geodox.flux.util.bst.BST;
import com.geodox.flux.util.bst.BSTNode;

/**
 * Created by GeoDoX on 2017-12-10.
 */
public class BSPTree
{
    private int width, height;

    private BST<Void> bst;

    // TODO: Will passing dungeon width and height work?
    public BSPTree(int width, int height)
    {
        this.width = width;
        this.height = height;

        bst = new BST<>(new BSTNode<Void, BSPNodeInfo>(null));
    }

    public BSPTree split(int n)
    {
        for (int i = 0; i < n; i++)
        {

        }

        return this;
    }
}
