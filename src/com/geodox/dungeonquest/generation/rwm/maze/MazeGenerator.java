package com.geodox.dungeonquest.generation.rwm.maze;

import com.geodox.dungeonquest.dungeon.Cell;
import com.geodox.dungeonquest.generation.Generator;
import com.geodox.flux.core.math.Vector2;

import java.util.Map;

/**
 * Created by GeoDoX on 2017-12-09.
 */
public class MazeGenerator implements Generator<Map<Cell, Vector2>>
{
    public MazeGenerator()
    {

    }

    @Override
    public Map<Cell, Vector2> generate()
    {
        return null;
    }
}
