package com.geodox.dungeonquest.generation.rwm;

import com.geodox.dungeonquest.dungeon.Cell;
import com.geodox.dungeonquest.dungeon.Dungeon;
import com.geodox.dungeonquest.dungeon.objects.Door;
import com.geodox.dungeonquest.dungeon.objects.Room;
import com.geodox.dungeonquest.generation.Generator;
import com.geodox.flux.core.math.Vector2;

import java.util.List;
import java.util.Map;

/**
 * Created by GeoDoX on 2017-12-09.
 */
public class RWMGenerator implements Generator<Dungeon>
{
    protected Dungeon dungeon;

    protected Map<Cell, Vector2> dungeonCells;

    protected List<Room> dungeonRooms;
    protected Map<Cell, Vector2> dungeonMaze;

    public RWMGenerator(Dungeon dungeon)
    {
        this.dungeon = dungeon;
    }

    @Override
    public Dungeon generate()
    {
        // Generate and Place Rooms
        // Generate Maze
        // Generate and Place Doors
        // Trim Maze
        // Place Maze

        return dungeon;
    }
}
