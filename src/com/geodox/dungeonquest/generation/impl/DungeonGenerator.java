package com.geodox.dungeonquest.generation.impl;

import com.geodox.dungeonquest.dungeon.Cell;
import com.geodox.dungeonquest.dungeon.Dungeon;
import com.geodox.dungeonquest.dungeon.objects.Door;
import com.geodox.dungeonquest.dungeon.objects.Room;
import com.geodox.flux.util.array.ArrayUtils;
import com.geodox.flux.util.compass.Direction;

import java.util.*;

/**
 * Created by GeoDoX on 2017-12-09.
 */
public class DungeonGenerator
{
    //region State Enum
    public enum State
    {
        READY,
        ROOMS_GENERATING,
        ROOMS_PLACING,
        MAZE_GENERATING,
        DOORS_FINDING,
        DOORS_PLACING,
        MAZE_TRIMMING,
        FINISHED
    }
    //endregion

    //region Variable Declarations
    private final long seed;
    private final Random random;
    private final Dungeon dungeon;

    private boolean initialized;
    private State state;
    //endregion

    //region Generator Variables
    private List<Room> dungeonRooms;
    private Map<Room, List<Door>> dungeonRoomDoorPossibilities;
    private Map<Room, Door> dungeonRoomDoors;
    private Map<Cell, Boolean> dungeonMazeCells;

    private int currentRoomIndex;
    private Room currentRoom;

    private List<Cell> unvisitedCells;
    private Stack<Cell> cellStack;
    private Cell currentCell;
    private Cell neighborCell;
    private Direction directionToNeighbor;

    private int potentialDoorCellIndex;
    private Cell potentialDoorCell;
    private Cell potentialDoorCellNeighbor;

    private int dungeonRoomDoorIndex;
    private Room dungeonRoom;
    private Door dungeonDoor;
    //endregion

    //region Constructors
    public DungeonGenerator(Dungeon dungeon)
    {
        this(dungeon, new Random().nextLong());
    }

    public DungeonGenerator(Dungeon dungeon, long seed)
    {
        this.seed = seed;
        this.random = new Random(seed);
        this.dungeon = dungeon;

        this.initialized = false;

        initialize();
    }
    //endregion

    //region Generator Methods
    private void initialize()
    {
        this.dungeonRooms = new ArrayList<>(0);
        this.dungeonRoomDoorPossibilities = new HashMap<>(0);
        this.dungeonRoomDoors = new HashMap<>(0);
        this.dungeonMazeCells = new HashMap<>(0);

        this.currentRoomIndex = 0;

        this.cellStack = new Stack<>();

        // Set the neighbors of all the cells
        setCellNeighbors(dungeon.cells(), dungeon.sizeX, dungeon.sizeY);

        // Create a list of unvisited cells, used in the algorithm, then
        // shuffle it deterministically
        this.unvisitedCells = new ArrayList<>(Arrays.asList(dungeon.cells()));
        Collections.shuffle(this.unvisitedCells, random);

        this.initialized = true;
        this.state = State.READY;
    }
    //endregion

    //region Condition Methods
    protected boolean allRoomsGenerated()
    {
        // Check if all rooms have been generated
        return true; // TODO: Implement Room Generation
    }

    protected boolean allRoomsPlaced()
    {
        // Check if all rooms have been placed
        return currentRoomIndex >= dungeonRooms.size();
    }

    protected boolean mazeCompletelyGenerated()
    {
        // Check if the maze has been completed generated
        return unvisitedCells.isEmpty();
    }

    protected boolean allDoorsFound()
    {
        // Check if all doors have been found
        return potentialDoorCellIndex >= dungeon.cells().length;
    }

    protected boolean allDoorsPlaced()
    {
        // Check if all doors have been placed
        return dungeonRoomDoorIndex >= dungeonRooms.size();
    }

    protected boolean mazeCompletelyTrimmed()
    {
        // Check if the maze has been completely trimmed
        return true; // TODO: Trim Maze
    }
    //endregion

    //region Algorithm Methods
    protected void generateRoom()
    {

    }

    protected void placeRoom()
    {
        if (dungeonRooms.size() != 0)
        {
            currentRoom = dungeonRooms.get(currentRoomIndex);

            if (currentRoom != null)
                currentRoom.place(dungeon.cells(), dungeon.sizeX, dungeon.sizeY, unvisitedCells);

            currentRoomIndex++;
        }
    }

    protected void generateMaze()
    {
       /*
        * Algorithm:
        * 1. Make the initial cell the current cell and mark it as visited
        * 2. While there are unvisited cells
        *   1. If the current cell has any neighbours which have not been visited
        *     1. Choose randomly one of the unvisited neighbours
        *     2. Push the current cell to the stack
        *     3. Remove the wall between the current cell and the chosen cell
        *     4. Make the chosen cell the current cell and mark it as visited
        *   2. Else if stack is not empty
        *     1. Pop a cell from the stack
        *     2. Make it the current cell
        */

        // If the current cell is null, assume the maze generation hasn't started
        if (currentCell == null)
        {
            // 1. Make the initial cell the current cell and mark it as visited
            currentCell = unvisitedCells.get(0);
            unvisitedCells.remove(currentCell);
        }

        // 2. If there are unvisited cells
        if (!mazeCompletelyGenerated())
        {
            // 2.1. If the current cell has any neighbours which have not been visited
            // 2.1.1. Choose randomly one of the unvisited neighbours
            directionToNeighbor = unvisitedNeighborDirection(currentCell, unvisitedCells);

            // NOTE: directionToNeighbor will be null if the current cell doesn't
            // have any neighbors which have not been visited
            if (directionToNeighbor == null)
            {
                // 2.2. Else if the stack is not empty
                if (!cellStack.empty())
                {
                    // 2.2.1. Pop a cell from the stack
                    // 2.2.2. Make it the current cell
                    currentCell = cellStack.pop();
                }
            }
            else
            {
                // 2.1.2. Push the current cell to the stack
                cellStack.push(currentCell);

                neighborCell = currentCell.neighbor(directionToNeighbor);

                // 2.1.3. Remove the wall between the current cell and the chosen cell
                currentCell.setWall(directionToNeighbor, false);
                neighborCell.setWall(directionToNeighbor.opposite(), false);

                currentCell.type(Cell.Type.MAZE); // TODO: THIS IS SOMEHOW NOT SETTING ALL CELLS IN THE MAZE TO TYPE MAZE

                // 2.1.4. Make the chosen cell the current cell and mark it as visited
                currentCell = neighborCell;
                unvisitedCells.remove(currentCell);
            }
        }
    }

    protected void findDoor()
    {
        if (potentialDoorCellIndex < dungeon.cells().length)
        {
            potentialDoorCell = dungeon.cells()[potentialDoorCellIndex];

            if (potentialDoorCell.type() != Cell.Type.MAZE)
            {
                potentialDoorCellIndex++;
                return;
            }

            for (Room room : dungeonRooms)
            {
                addDoorPossibilityToList(potentialDoorCell, Direction.NORTH, room);
                addDoorPossibilityToList(potentialDoorCell, Direction.EAST, room);
                addDoorPossibilityToList(potentialDoorCell, Direction.SOUTH, room);
                addDoorPossibilityToList(potentialDoorCell, Direction.WEST, room);
            }

            potentialDoorCellIndex++;
        }
    }

    protected void placeDoor()
    {
        if (dungeonRooms.size() != 0)
        {
            dungeonRoom = dungeonRooms.get(dungeonRoomDoorIndex);
            dungeonDoor = randomDungeonRoomDoor(dungeonRoom);

            if (dungeonDoor != null)
                dungeonRoomDoors.put(dungeonRoom, dungeonDoor);

            dungeonRoomDoorIndex++;
        }
    }

    protected void trimMaze()
    {

    }
    //endregion

    //region Generation Util Methods
    private void setCellNeighbors(Cell[] cells, int width, int height)
    {
        int cellIndex;
        int neighborIndex;
        Direction neighborDirection;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                cellIndex = ArrayUtils.index2DTo1D(x, y, width);

                // If possible, add WEST neighbor
                if (x != 0)
                {
                    neighborDirection = Direction.WEST;
                    neighborIndex = ArrayUtils.index2DTo1D((int) (x + neighborDirection.offset().x), (int) (y + neighborDirection.offset().y), width);
                    cells[cellIndex].addNeighbor(neighborDirection, cells[neighborIndex]);
                }

                // If possible, add EAST neighbor
                if (x != width - 1)
                {
                    neighborDirection = Direction.EAST;
                    neighborIndex = ArrayUtils.index2DTo1D((int) (x + neighborDirection.offset().x), (int) (y + neighborDirection.offset().y), width);
                    cells[cellIndex].addNeighbor(neighborDirection, cells[neighborIndex]);
                }

                // If possible, add NORTH neighbor
                if (y != 0)
                {
                    neighborDirection = Direction.NORTH;
                    neighborIndex = ArrayUtils.index2DTo1D((int) (x + neighborDirection.offset().x), (int) (y + neighborDirection.offset().y), width);
                    cells[cellIndex].addNeighbor(neighborDirection, cells[neighborIndex]);
                }

                // If possible, add SOUTH neighbor
                if (y != height - 1)
                {
                    neighborDirection = Direction.SOUTH;
                    neighborIndex = ArrayUtils.index2DTo1D((int) (x + neighborDirection.offset().x), (int) (y + neighborDirection.offset().y), width);
                    cells[cellIndex].addNeighbor(neighborDirection, cells[neighborIndex]);
                }
            }
        }
    }

    private Direction unvisitedNeighborDirection(Cell mazeCell, List<Cell> unvisitedCells)
    {
        List<Map.Entry<Direction, Cell>> unvisitedNeighbors = new ArrayList<>(0);

        for (Map.Entry<Direction, Cell> neighborEntry : mazeCell.neighbors().entrySet())
        {
            if (unvisitedCells.contains(neighborEntry.getValue()))
                unvisitedNeighbors.add(neighborEntry);
        }

        if (unvisitedNeighbors.size() > 0)
        {
            Map.Entry<Direction, Cell> unvisitedNeighbor = unvisitedNeighbors.get(random.nextInt(unvisitedNeighbors.size()));
            return unvisitedNeighbor.getKey();
        }

        return null;
    }

    private boolean addDoorPossibilityToList(Cell mazeCell, Direction directionToNeighbor, Room room)
    {
        potentialDoorCellNeighbor = mazeCell.neighbor(directionToNeighbor);

        if (potentialDoorCellNeighbor == null || potentialDoorCellNeighbor.type() != Cell.Type.ROOM)
            return false;

        if (room.containsCell(potentialDoorCellNeighbor))
        {
            dungeonRoomDoorPossibilities.get(room).add(new Door(mazeCell, potentialDoorCellNeighbor, directionToNeighbor));

            return true;
        }

        return false;
    }

    private Door randomDungeonRoomDoor(Room room)
    {
        List<Door> possibleDoors = dungeonRoomDoorPossibilities.get(room);

        if (possibleDoors.size() > 0)
            return possibleDoors.get(random.nextInt(possibleDoors.size()));

        return null;
    }

    // TODO: Remove this method and generate rooms
    public void addRoom(Room room)
    {
        this.dungeonRooms.add(room);
    }
    //endregion

    //region Verify Generator State
    private void verifyState()
    {
        switch (this.state)
        {
            case READY:
                this.state = State.ROOMS_GENERATING;
                break;
            case ROOMS_GENERATING:
                if (allRoomsGenerated())
                {
                    this.state = State.ROOMS_PLACING;
                }
                break;
            case ROOMS_PLACING:
                if (allRoomsPlaced())
                {
                    this.state = State.MAZE_GENERATING;
                }
                break;
            case MAZE_GENERATING:
                if (mazeCompletelyGenerated())
                {
                    this.state = State.DOORS_FINDING;

                    setCellNeighbors(dungeon.cells(), dungeon.sizeX, dungeon.sizeY);

                    for (Room room : this.dungeonRooms)
                        this.dungeonRoomDoorPossibilities.put(room, new ArrayList<>(0));

                    this.potentialDoorCellIndex = 0;
                }
                break;
            case DOORS_FINDING:
                if (allDoorsFound())
                {
                    this.state = State.DOORS_PLACING;

                    this.dungeonRoomDoorIndex = 0;
                }
                break;
            case DOORS_PLACING:
                if (allDoorsPlaced())
                {
                    this.state = State.MAZE_TRIMMING;

                    dungeon.doors(new ArrayList<>(dungeonRoomDoors.values()));
                }
                break;
            case MAZE_TRIMMING:
                if (mazeCompletelyTrimmed())
                {
                    this.state = State.FINISHED;
                }
                break;
        }
    }
    //endregion

    //region Generate Next Step in Dungeon
    public void step()
    {
        verifyState();

        switch (this.state)
        {
            case ROOMS_GENERATING:
                generateRoom();
                break;
            case ROOMS_PLACING:
                placeRoom();
                break;
            case MAZE_GENERATING:
                generateMaze();
                break;
            case DOORS_FINDING:
                findDoor();
                break;
            case DOORS_PLACING:
                placeDoor();
                break;
            case MAZE_TRIMMING:
                trimMaze();
                break;
        }
    }
    //endregion

    //region Generator Generate/Getters
    public void generate()
    {
        while (!finished())
            step();
    }

    public boolean ready()
    {
        return this.initialized;
    }

    public boolean finished()
    {
        return this.state == State.FINISHED;
    }

    public long seed()
    {
        return this.seed;
    }
    //endregion
}
