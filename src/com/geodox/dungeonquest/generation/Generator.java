package com.geodox.dungeonquest.generation;

/**
 * Created by GeoDoX on 2017-12-09.
 */
public interface Generator<T>
{
    T generate();
}
