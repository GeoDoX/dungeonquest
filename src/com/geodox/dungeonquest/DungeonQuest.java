package com.geodox.dungeonquest;

import com.geodox.dungeonquest.states.TestState;
import com.geodox.flux.FluxGame;
import com.geodox.flux.core.gamestate.GameStateManager;
import com.geodox.flux.event.Event;
import com.geodox.flux.event.EventDispatcher;
import com.geodox.flux.event.fluxevents.game.GameStartedEvent;
import com.geodox.flux.util.logger.Logger;

/**
 * Created by GeoDoX on 2017-12-04.
 */
public class DungeonQuest extends FluxGame
{
    DungeonQuest()
    {
        setFluxProperties();

        EventDispatcher.getInstance().registerEventListener(this, GameStartedEvent.class);
    }

    @Event.EventHandler
    public void onGameStarted(GameStartedEvent event)
    {
        GameStateManager.getInstance().push(new TestState());
    }

    private void setFluxProperties()
    {
        fluxGameProperties.gameTitle = "DungeonQuest";
        fluxGameProperties.gameAuthor = "GeoDoX";

        fluxGameProperties.windowTitle = fluxGameProperties.gameTitle;

        fluxGameProperties.shouldLog = true;
        fluxGameProperties.logsToKeep = 5;
        fluxGameProperties.shouldPrintLog = true;
        fluxGameProperties.gameTag = new Logger.LoggerTag(fluxGameProperties.gameTitle);

        fluxGameProperties.shouldLimitFPS = true;
        fluxGameProperties.shouldPrintFPS_UPS = false;
        fluxGameProperties.targetFPS = 60;
        fluxGameProperties.targetUPS = 120;

        fluxGameProperties.windowWidth = 960;
        fluxGameProperties.windowHeight = 960;
        fluxGameProperties.fullscreen = false;
        fluxGameProperties.canResize = false;

        fluxGameProperties.shouldProfile = false;
    }

    public static class Launcher
    {
        private static DungeonQuest game;

        public static void main(String[] args)
        {
            game = new DungeonQuest();
            game.start();
        }
    }
}
