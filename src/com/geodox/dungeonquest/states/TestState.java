package com.geodox.dungeonquest.states;

import com.geodox.dungeonquest.DungeonQuest;
import com.geodox.dungeonquest.dungeon.Dungeon;
import com.geodox.dungeonquest.generation.impl.DungeonGenerator;
import com.geodox.dungeonquest.dungeon.objects.Room;
import com.geodox.flux.core.gamestate.GameState;

import java.awt.Graphics2D;

/**
 * Created by GeoDoX on 2017-12-08.
 */
public class TestState extends GameState
{
    private Dungeon dungeon;

    private int cellSize;

    @Override
    protected void init()
    {
        dungeon = new Dungeon(40, 40);

        cellSize = (int) (DungeonQuest.getInstance().getWindowHandle().getSize().x / dungeon.sizeX);

        DungeonGenerator generator = new DungeonGenerator(dungeon, 0);

        generator.addRoom(new Room(5, 3, 12, 8));
        generator.addRoom(new Room(20, 15, 4, 7));
        generator.addRoom(new Room(3, 13, 9, 10));
        generator.addRoom(new Room(32, 1, 6, 8));
        generator.addRoom(new Room(13, 25, 19, 12));

        generator.generate();
    }

    @Override
    public void update(long l)
    {

    }

    @Override
    public void render(Graphics2D g)
    {
        dungeon.render(g, cellSize);
    }

    @Override
    public void dispose()
    {

    }
}
